from math import sqrt
import random

def dataSet(ruta):
	rownames = []
	colnames = []
	data = []

	tmp = (open(ruta)).readlines()

	colnames = tmp[0].strip().split("\t")[1:];

	for d in tmp[1:]:
		dataTmp = d.strip().split("\t")
		rownames.append(dataTmp[0])
		data.append([float(dTmp) for dTmp in dataTmp[1:]])

	return rownames,colnames,data

class cluster:
	def __init__(self,vec,id,izq=None,der=None,distancia=None):
		self.vec = vec
		self.id = id
		self.izq = izq
		self.der = der
		self.distancia = distancia

def pearson(v1,v2):
	#print v1

	sum1 = sum(v1)
	sum2 = sum(v2)

	sumSq1 = sum([pow(d,2) for d in v1])
	sumSq2 = sum([pow(d,2) for d in v2])

	prod = sum([v1[i]*v2[i] for i in range(len(v1))])

	num = prod - (sum1 * sum2/len(v1))
	den = sqrt((sumSq1 - pow(sum1,2)/len(v1)) * (sumSq2 - pow(sum2,2)/len(v1)))

	if den == 0: return 0

	return 1.0 - num/den

def doClusterKMeans(rows,k,distancia=pearson):
	#General : Valor minimo y maximo de cada objecto por objeto de estudio
	#Especifico: Valor minimo y maximo de cada palabra por Blog
	ranges = [(min(row[i] for row in rows),max(row[i] for row in rows)) for i in range(len(rows[0]))]
	
	#posiciones random
	centroids = [[random.random() * (ranges[i][1] - ranges[i][0]) + ranges[i][0] for i in range(len(rows[0]))] for j in range(k)]	
	
	lastMatches = None

	for i in range(100):
		if i % 20 == 0:
			print "Iteracion %d" % i

		clusterMatches = [[] for _ in range(k)]

		for index in range(len(rows)):
			distanciaMenor = 0;
			dist = distancia(centroids[0],rows[index])
			for clusterIndex in range(len(centroids)):
				distanciaTmp = distancia(centroids[clusterIndex],rows[index])
				if distanciaTmp < dist: distanciaMenor = clusterIndex  

			clusterMatches[distanciaMenor].append(index)

		if clusterMatches == lastMatches: break
		lastMatches = clusterMatches

		#Por cada centroids
		for centroidIndex in range(len(centroids)):
			#lista de average
			avg = [0.0] * len(rows[0])
			#por cada Match Index
			for matchIndex in clusterMatches[centroidIndex]:
				#Por cada columna
				for index in  range(len(rows[0])):
					#agrego
					avg[index] += rows[matchIndex][index]

				for index in range(len(rows[0])):
					avg[index] /= len(clusterMatches[centroidIndex])

			centroids[centroidIndex] = avg
				
	return lastMatches		

def printClusterKMeans(clusters,rowNames):
	out = open("printKMeans.txt","w")
	out.write("Data por Cluster\n\n")
	for clusterIndex in range(len(clusters)):
		out.write("Cluster %d\n" % clusterIndex)
		for indexBlog in clusters[clusterIndex]:
			out.write("%s\n" % rowNames[indexBlog])
		out.write("\n\n")


def doCluster(dataRow,distancia=pearson):
	distancias = {}
	clusters = [cluster(dataRow[i],i) for i in range(len(dataRow))]
	cantidadColumnas = len(dataRow[0])
	currentId = -1

	contador = 1

	while len(clusters) > 1:
		if contador % 100 == 0:
			print "Cargando..."

		contador+=1

		parejaMenor = (0,1)
		masCercano = distancia(clusters[parejaMenor[0]].vec,clusters[parejaMenor[1]].vec)

		for i in range(len(clusters)):
			for j in range(i+1,len(clusters)):
				if (clusters[i].id,clusters[j].id) not in distancias.keys():
					distancias[(clusters[i].id,clusters[j].id)] = distancia(clusters[i].vec,clusters[j].vec)

				distanciaTmp =  distancias[(clusters[i].id,clusters[j].id)]

				if distanciaTmp < masCercano:
					masCercano = distanciaTmp
					parejaMenor = (i,j)

		mergeValores = [(clusters[parejaMenor[0]].vec[dataIndex]+clusters[parejaMenor[1]].vec[dataIndex])/2 for dataIndex in range(cantidadColumnas)]
		mCluster = cluster(mergeValores,currentId,izq=clusters[parejaMenor[0]],der=clusters[parejaMenor[1]],distancia=masCercano)

		del clusters[parejaMenor[1]]
		del clusters[parejaMenor[0]]

		currentId -= 1
		clusters.append(mCluster)

	return clusters[0]

escritor = file("print3.txt","w")
def printCluster(cluster,rownames,indent=0,padre=None):
	
	if cluster.id < 0:
		escritor.write(("\t"*indent)+"-\n")
	else:
		escritor.write(("\t"*indent)+"%s\n" % rownames[cluster.id])

	if cluster.izq is not None:
		printCluster(cluster.izq,rownames,(indent+1),padre=cluster)

	if cluster.der is not None:
		printCluster(cluster.der,rownames,(indent+1),padre=cluster)


